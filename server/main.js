const shell = require('shelljs');
const command = `ps aux | jq -sR '[sub("\n$";"") | splits("\n") | sub("^ +";"") | [splits(" +")]] | .[0] as $header | .[1:] | [.[] | [. as $x | range($header | length) | {"key": $header[.], "value": $x[.]}] | from_entries]'`;
const express = require('express');
const app = express();
const port = 8080;
const cors = require('cors');
app.use(cors({
    "origin": "*",
    "methods": "GET,HEAD,PUT,PATCH,POST,DELETE",
    "preflightContinue": false,
    "optionsSuccessStatus": 204
  }));
// app.options('*', cors());
app.get('/monitor', (req, res) => {
    console.log("monitor")
    // res.send({data: "string"});
    res.send(getSystemProcessListAsJson())
})

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})
 function getSystemProcessListAsJson() {
    let process =  shell.exec(command).stdout;
    // console.log(JSON.parse(process));
    return {data: JSON.parse(process)}; 
}