import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { useState, useEffect } from 'react';
import Tooltip from '@material-ui/core/Tooltip';
const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
  command:{
    maxWidth: "200px",
    textOverflow: "ellipsis",
    overflow: "hidden"
  }
});
// CMD: "/usr/local/Cellar/node/16.4.1/bin/node"
// PID: "89958"
// TIME: "0:19.13"
// TTY: "ttys001"

// %CPU: "0.0"
// %MEM: "0.1"
// COMMAND: "/Library/Tanium/TaniumClient/TaniumCX"
// PID: "92794"
// RSS: "16980"
// STARTED: "4:52pm"
// STAT: "S"
// TIME: "0:03.69"
// TT: "??"
// USER: "root"


function createData(CMD, PID, TIME, TTY) {
  return { CMD, PID, TIME, TTY };
}

// const rows = [
//   createData('Frozen yoghurt', 159, 6.0, 24, 4.0),
//   createData('Ice cream sandwich', 237, 9.0, 37, 4.3),
//   createData('Eclair', 262, 16.0, 24, 6.0),
//   createData('Cupcake', 305, 3.7, 67, 4.3),
//   createData('Gingerbread', 356, 16.0, 49, 3.9),
// ];

export default function TableComponent() {
  const classes = useStyles();
  const [processList, setProcessList] = useState([]);
  useEffect(() => { fetchProcessList(setProcessList) }, []);
  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell width="200" >COMMAND</TableCell>
            <TableCell align="right">PID</TableCell>
            <TableCell align="right">TIME&nbsp;</TableCell>
            <TableCell align="right">USER&nbsp;</TableCell>
            <TableCell align="right">%CPU&nbsp;</TableCell>
            <TableCell align="right">%MEM&nbsp;</TableCell>
            <TableCell align="right">START&nbsp;</TableCell>
            <TableCell align="right">STAT&nbsp;</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {processList.map((p)=>mapRow(p)).map((processList) => (
            <TableRow key={processList.PID}>
              <TableCell title={processList.COMMAND} className={classes.command} component="th" scope="row">
                <span title={processList.COMMAND}>
                {processList.COMMAND}
                </span>
              </TableCell>
              <TableCell align="right">{processList.PID}</TableCell>
              <TableCell align="right">{processList.TIME}</TableCell>
              <TableCell align="right">{processList.USER}</TableCell>
              <TableCell align="right">{processList["%CPU"]}</TableCell>
              <TableCell align="right">{processList["%MEM"]}</TableCell>
              <TableCell align="right">{processList.START}</TableCell>
              <TableCell align="right">{processList.STAT}</TableCell>
             
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
function mapRow(row){
  // console.log(row);
  return row;
}

function fetchProcessList(setList) {
  fetch('http://localhost:8080/monitor')
  
  .then(Response => Response.json())
  .then((response) => {
    console.log(response)
    setList(response.data)
  }, err => {
    console.log(err);
  });


}