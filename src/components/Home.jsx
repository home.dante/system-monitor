import React from 'react';
import TableComponent from './TableComponent';
import { makeStyles } from '@material-ui/core/styles';
import { auto } from 'async';
const useStyles = makeStyles({
    container: {
      margin: "0 auto",
      display: "block",
      width: "80%",
      marginTop: "100px"
    },
  });
function Home(props) {
    const classes = useStyles();
    return (
        <div>
            <div className={classes.container}>

            <TableComponent />
            </div>
        </div>
    );
}

export default Home;